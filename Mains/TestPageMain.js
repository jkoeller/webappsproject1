function initialize() {
	document.NODE_SIZE = 30;
	canvas = document.getElementById('canv')
	context = canvas.getContext('2d');
	let structList = makeStructure();
	structure = structList[0];
	let refStructure = structList[1];
	let nodes = []; //TODO once I have the jquery stuff up and running. Will involve a function to make an array of nodes.
	var selected = null;
	canvas.onpressed = function (e) {
		var x = e.offsetX - canvas.width/2; //Can pull how to do this from the n-body program
		var y = e.offsetY - canvas.height/2;
		if (nodes[0].contains(x,y) && nodes.length > 0)
		{
			selected = nodes[0];
			nodes.slice(0,1);
			nodes[0].hardUpdate(selected.x,selected.y);
		}
	}
	canvas.ondragged = function (e) {
		if (selected !== null)
		{
			var x = e.offsetX - canvas.width/2;
			var y = e.offsetY - canvas.width/2;
			selected.updatePos(x,y);
		}
	}
	canvas.onreleased = function (e) {
		if (selected !== null)
		{
			var min = structure.children[structure.children.length - 1].x**2 + structure.children[structure.children.length - 1].y**2;
			var closest = structure.children[structure.children.length -1];
			var index = structure.children.length - 1;
			for (var i = structure.children.length - 1; i >= 0; i--)
			{
				dist = structure.children[i].x**2 + structure.children[i].y**2;
				if (dist < min)
				{
					closest = structure.children[i];
					min = dist;
					index = i;
				}
			}
			structure.push(selected.data,index);
			refStructure.push(selected.data);
			selected = null;
		}
	}
	setInterval(runLoop,50);
}



window.onload = initialize;

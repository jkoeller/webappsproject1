window.onload = init;

function init() {
    document.NODE_SIZE = 30;
    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');

    bst = new BST(400, 400, null);
    bst.push(7);
    bst.push(3);
    bst.push(5);
    bst.push(2);
    bst.push(1);
    bst.push(10);
    bst.push(14);

    // bst.push(6);
    // bst.push(9);
    bst.delete(bst.root, 7);

    setInterval(update,50);
}

function update() {
    ctx.fillStyle = "white";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    bst.refresh(ctx,4);
}

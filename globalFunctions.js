
function makeStructure(name, order) {
	var struc = null;
	var refstruc = null;
	if (name === "bst")
	{
		console.log("new bst");
	  	struc = new BST(300, 0, null);
	  	refstruc = new BST(600,0, null);
	}
	else
	{
		var ordering = null;
		if (order === "pq")
		{
			ordering = new PriorityQueueOrdering();
		}
		else if (order === "q")
		{
			console.log("queuing");
			ordering = new QueueOrdering();
		}
		else
		{
			console.log("stack ordering");
			ordering = new StackOrdering();
		}
		if (name === "sll")
		{
			console.log("singlylinking");
			struc = new SinglyLinkedList(400,200,ordering);
			refstruc = new SinglyLinkedList(400,600,ordering);
		}
		else
		{
			console.log("doublylinking");
			struc = new DoublyLinkedList(400,200,ordering);
			refstruc = new DoublyLinkedList(400,600,ordering);

		}
	}
	return [struc,refstruc];
}

function parseStructure(name, order) {
	var struc = null;
	var refstruc = null;
	if (name === "0")
	{
		console.log("new bst");
	  	struc = new BST(300,0, null);
	  	refstruc = new BST(600,0);
	}
	else
	{
		var ordering = null;
		if (order === "1")
		{
			ordering = new PriorityQueueOrdering();
		}
		else if (order === "2")
		{
			console.log("queuing");
			ordering = new QueueOrdering();
		}
		else
		{
			console.log("stack ordering");
			ordering = new StackOrdering();
		}
		if (name === "1")
		{
			console.log("singlylinking");
			struc = new SinglyLinkedList(400,200,ordering);
			refstruc = new SinglyLinkedList(400,600,ordering);
		}
		else
		{
			console.log("doublylinking");
			struc = new DoublyLinkedList(400,200,ordering);
			refstruc = new DoublyLinkedList(400,600,ordering);

		}
	}
	return [struc,refstruc];
}
    function runLoop2() {
        context.fillStyle = "white";
        context.fillRect(0,0,canvas.width,canvas.height);
        structure.refresh(context,4);
        if (nodes !== [] && nodes !== null && nodes[0] !== undefined) {nodes[0].refresh(context,4);}
        if (showingRef) {refStructure.refresh(context,1000000);}
    }
    function callRunLoop2() {
        setInterval(runLoop2,50);
    }

function runLoop() {
	context.fillStyle = "white";
	context.fillRect(0,0,canvas.width,canvas.height);
	structure.refresh(context,4);
	//console.log(structure.size());
}
function initialize() {
	document.NODE_SIZE = 30;
	canvas = document.getElementById('canv');
	context = canvas.getContext('2d');
}
function callRunLoop() {
	setInterval(runLoop,50);
}

function stringToStructure(str) {
        var map ={};
        let ordering = str[1];
        let type = str[0];
        let id = str.slice(2,str.indexOf(':')+1);
        let nodeStr = str.slice(str.indexOf(':')+1,str.indexOf(';'));
        let nodeStrs = nodeStr.split(',');
        let pgName = str.slice(str.indexOf(';')+1);
        let nodes = [];
        for (var i = nodeStrs.length - 1; i >= 0; i--) {
                nodes.push(Number(nodeStrs[i]));
        }
        let struct = parseStructure(type,ordering)[0];
        struct.insertArray(nodes);
        map.structure = struct;
        map.pgName = pgName;
        map.structId = id;
        return map;

}
function structureToString(structure,pgid,pgName) {
        return structure.printInfo()+":"+pgid+structure.printChildren()+";"+pgName;
}


class DoublyLinkedList extends DataStructure {
	constructor(x,y,ordering) {
		super(x,y);
		this.cent = new CentNode(x,y-50,null); //Position might be refined later.
		this.ordering = ordering;
		this.shift = document.NODE_SIZE*2;
	}

	pop() {
		let index = this.ordering.remove();
		this.children[index].prev.updateNext(this.children[index].next);
		this.children[index].next.updatePrev(this.children[index].prev);
		this.children.splice(index,1);
		this._movePointers();
		this._updateChildrenPositions();

	}
	push(elem,hardIndex = -1,x=300,y=150,diff=0) {
        var index = 0;
        if (diff >0)
        {
            index = this.ordering.insert(elem, this.children, hardIndex)+1;
        }
        else
        {
            index = this.ordering.insert(elem, this.children, hardIndex);
        }		var nodeX = 0;
		if (this.children.length === 0)
		{
			nodeX = this.x;
		}
		else {
			nodeX = this.children[index] - document.NODE_SIZE;
		}
		let nodeY = this.y;
		let node = new DLLNode(x,y,elem);
		this.children.splice(index,0,node);
		this._movePointers();
		this._updateChildrenPositions();
	}

	_movePointers() {
		for (var child=0; child < this.children.length-1;child++)
		{
			this.children[child].updateNext(this.children[child+1]);
			// console.log("setting next of " + this.children[child].data + " as " +this.children[(child+1)].data);
		}
		for (var child = this.children.length-1; child > 0;child--)
		{
			this.children[child].updatePrev(this.children[child-1]);
			// console.log("setting prev of " + this.children[child].data + " as " +(this.children[child-1].data));

		}
		this.children[0].updatePrev(this.cent);
		this.children[this.children.length-1].updateNext(this.cent);
		this.cent.updatePrev(this.children[0]);
		this.cent.updateNext(this.children[this.children.length-1]);
	}

	refresh(context,step) {
		super.refresh(context,step);
		this.cent.refresh(context,step);
	}

	insertArray(arr) {
		for (var i = arr.length - 1; i >= 0; i--) {
			this.push(arr[i]);
		}
	}
	size() {
		return this.children.length;
	}

	_updateChildrenPositions() {
		for (var child = 0; child < this.children.length; child++) {
			let midpt = this.children.length/2;
			let newX = (this.x+(child-midpt)*this.shift + document.NODE_SIZE);
			this.children[child].updatePos(newX,this.y);
		}
	}
    printInfo() {
        return "3"+this.ordering.print();
    }
    printChildren() {
        var str = "";
        for (var i = this.children.length - 1; i >= 0; i--) {
                str = str+this.children[i].data+",";
        }
        return str.slice(0,str.length-1);
    }

	// draw(context) {
	// 	cent.draw(context);
	// }
}
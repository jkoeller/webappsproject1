class Pointer {
	constructor(originX,originY,destX,destY) {
		this.originX = originX;
		this.originY = originY;
		this.destX = destX;
		this.destY = destY;
		this.futureOriginX = originX;
		this.futureOriginY = originY;
		this.futureDestX = destX;
		this.futureDestY = destY;
	}

	updateDest(x,y) {
		this.futureDestX = x;
		this.futureDestY = y;
	}
	updateOrigin(x,y) {
		this.futureOriginX = x;
		this.futureOriginY = y;
	}

	refresh(context,step) {
		if (this.originX !== this.futureOriginX) {
			if (this.originX > this.futureOriginX) {
				this.originX -= Math.min(step,this.originX - this.futureOriginX);
			}
			else {
				this.originX += Math.min(step,this.futureOriginX-this.originX);
			}
		}
		if (this.originY !== this.futureOriginY) {
			if (this.originY > this.futureOriginY) {
				this.originY -= Math.min(step,this.originY - this.futureOriginY);
			}
			else {
				this.originY += Math.min(step,this.futureOriginY-this.originY);
			}
		}
		if (this.destX !== this.futureDestX) {
			if (this.destX > this.futureDestX) {
				this.destX -= Math.min(step,this.destX - this.futureDestX);
			}
			else {
				this.destX += Math.min(step,this.futureDestX-this.destX);
			}
		}
		if (this.destY !== this.futureDestY) {
			if (this.destY > this.futureDestY) {
				this.destY -= Math.min(step,this.destY - this.futureDestY);
			}
			else {
				this.destY += Math.min(step,this.futureDestY-this.destY);
			}
		}
	}
	draw(context) {
		context.fillStyle = "black";
        context.beginPath();
        context.moveTo(this.originX,this.originY);
        context.lineTo(this.destX,this.destY);
        context.stroke();
        let dir = Math.atan2(this.originX-this.destX,-this.originY+this.destY);
        let topDir = dir - 3*3.14159265/4;
        let lowDir = dir - 3.14159265/4;
        let pointLength = 7;
        context.beginPath();
        context.moveTo(this.destX,this.destY);
        context.lineTo(pointLength*Math.cos(topDir)+this.destX,pointLength*Math.sin(topDir)+this.destY);
        context.stroke();
        context.beginPath();
        context.moveTo(this.destX,this.destY);
        context.lineTo(pointLength*Math.cos(lowDir)+this.destX,pointLength*Math.sin(lowDir)+this.destY);
        context.stroke();
	}
}
class SLLNode extends Node {
    constructor(x,y,data) {
        super(x,y,data);
        this.next = this;
        this.pointers[0] = new Pointer(this.getRightAnchor()[0],this.getRightAnchor()[1],
        this.getRightAnchor()[0],this.getRightAnchor()[1]);
    }
    updateNext(node) {
        this.next = node;
    }
    getRightAnchor() {
        return [this.x+document.NODE_SIZE/2,this.y+document.NODE_SIZE];
    }
    getLeftAnchor() {
        return [this.x-document.NODE_SIZE/2,this.y+document.NODE_SIZE];
    }
    refresh(context,step) {
        this.pointers[0].updateOrigin(this.getRightAnchor()[0],this.getRightAnchor()[1]);
        this.pointers[0].updateDest(this.next.getLeftAnchor()[0],this.next.getLeftAnchor()[1]);
        super.refresh(context,step);
        this.draw(context);
    }
    lastRefresh(context, step) {
        super.refresh(context,step);
        this.draw(context);
    }
    deletePointer() {
        if(this.pointers.length>0) this.pointers.splice(0, 1);
    }
    addPointer() {
        this.pointers[0] = new Pointer(this.getRightAnchor()[0],this.getRightAnchor()[1],
        this.getRightAnchor()[0],this.getRightAnchor()[1]);
    }

/*
    refresh(context,step) {
        if (this.x!==this.futureX) {
            // console.log(this.x + "," + this.futureX);
            if (this.x > this.futureX) {
                this.x -= Math.min(step,this.x-this.futureX);
            }
            else {
                this.x += Math.min(step,this.futureX-this.x);
            }
        }
        if (this.y!==this.futureY) {
            if (this.y > this.futureY) {
                this.y -= Math.min(step,this.y-this.futureY);
            }
            else {
                this.y += Math.min(step,this.futureY-this.y);
            }
        }
        for (var i = this.pointers.length - 1; i >= 0; i--) {
            this.pointers[i].refresh(context,step);
        }
        this.draw(context);
    }
*/
    draw(context) {
        context.fillStyle = "black";
        let sz = document.NODE_SIZE/2;
        //context.rect(this.x-sz,this.y+sz,document.NODE_SIZE,document.NODE_SIZE);
        //context.stroke();
        context.beginPath();
        context.moveTo(this.x-sz, this.y+sz);
        context.lineTo(this.x-sz+document.NODE_SIZE, this.y+sz);
        context.stroke();
        context.beginPath();
        context.moveTo(this.x-sz, this.y+sz);
        context.lineTo(this.x-sz, this.y+sz+document.NODE_SIZE);
        context.stroke();
        context.beginPath();
        context.moveTo(this.x+sz, this.y+3*sz);
        context.lineTo(this.x-sz, this.y+sz+document.NODE_SIZE);
        context.stroke();       
        context.moveTo(this.x+sz, this.y+3*sz);
        context.lineTo(this.x+sz, this.y+sz);
        context.stroke();
        context.fillText(this.data,this.x,this.y+30);
        if(this.pointers.length>0) this.pointers[0].draw(context);
        for (var i = this.pointers.length - 1; i >= 0; i--) {
            this.pointers[i].draw(context);
        }
    }
}


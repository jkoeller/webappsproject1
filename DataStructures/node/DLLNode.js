class DLLNode extends Node {
	constructor(x,y,data) {
		super(x,y,data);
		this.next = this;
		this.prev = this;
		this.pointers[0] = new Pointer(this.getLeftAnchor()[0],this.getLeftAnchor()[1],this.getLeftAnchor()[0],this.getLeftAnchor()[1]);
		this.pointers[1] = new Pointer(this.getRightAnchor()[0],this.getRightAnchor()[1],this.getRightAnchor()[0],this.getRightAnchor()[1]);
	}

	updateNext(node) {
		this.next = node;
	}

	updatePrev(node) {
		this.prev = node;
	}

	getRightAnchor() {
		return [this.x+document.NODE_SIZE/2,this.y+document.NODE_SIZE]; //Might not be legit JS?????
	}

	getLeftAnchor() {
		return [this.x-document.NODE_SIZE/2,this.y+document.NODE_SIZE]; //Might not be legit JS?????
	}

	refresh(context,step) {
		this.pointers[0].updateOrigin(this.getLeftAnchor()[0],this.getLeftAnchor()[1]+5);
		this.pointers[0].updateDest(this.prev.getRightAnchor()[0],this.prev.getRightAnchor()[1]+5);
		this.pointers[1].updateOrigin(this.getRightAnchor()[0],this.getRightAnchor()[1]-5);
		this.pointers[1].updateDest(this.next.getLeftAnchor()[0],this.next.getLeftAnchor()[1]-5);
		super.refresh(context,step);
		this.draw(context);
	}
}

class CentNode extends Node {
	constructor(x,y,data) {
		super(x,y,data);
		this.next = this;
		this.prev = this;
		this.pointers[0] = new Pointer(this.getLeftAnchor()[0],this.getLeftAnchor()[1],this.getLeftAnchor()[0],this.getLeftAnchor()[1]);
		this.pointers[1] = new Pointer(this.getRightAnchor()[0],this.getRightAnchor()[1],this.getRightAnchor()[0],this.getRightAnchor()[1]);
	}

	updateNext(node) {
		this.next = node;
	}

	updatePrev(node) {
		this.prev = node;
	}

	getLeftAnchor() {
		return [this.x+document.NODE_SIZE/2,this.y+document.NODE_SIZE]; //Might not be legit JS?????
	}

	getRightAnchor() {
		return [this.x-document.NODE_SIZE/2,this.y+document.NODE_SIZE]; //Might not be legit JS?????
	}

	refresh(context,step) {
		this.pointers[0].updateOrigin(this.getRightAnchor()[0],this.getRightAnchor()[1]-5);
		this.pointers[0].updateDest(this.prev.getLeftAnchor()[0],this.prev.getLeftAnchor()[1]-5);
		this.pointers[1].updateOrigin(this.getLeftAnchor()[0],this.getLeftAnchor()[1]+5);
		this.pointers[1].updateDest(this.next.getRightAnchor()[0],this.next.getRightAnchor()[1]+5);
		super.refresh(context,step);
		this.draw(context);
	}
}
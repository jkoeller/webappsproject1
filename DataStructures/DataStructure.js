class DataStructure {
	constructor(x,y) {
		this.x = x;
		this.y = y;
		this.children = [];
	}

	refresh(context,step) {
		for (var child = this.children.length - 1; child >= 0; child--) {
			this.children[child].refresh(context,step);
		}
	}
	push(elem) {

	}
	insertArray(arr) {
		for (var i = 0; i <= arr.length-1; i++) {
			this.push(arr[i]);
		}
	}

	insertStatic(index,elem) {
		this.children[index] = elem;
	}

	size() {
		return this.children.length;
	}

	equals(other) {
		for (var i = this.children.length - 1; i >= 0; i--) {
			if (this.children[i].data !== other.children[i].data) {return false;}
		}
		return this.children.length === other.children.length;
	}
	neq(other) {
		return !this.equals(other);
	}

	printInfo() {

	}

	printChildren() {
		
	}

}
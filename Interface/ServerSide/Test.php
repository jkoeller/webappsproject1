<?php
session_start();

include_once "Connection.php";

function get_tests($userId) {
	global $conn;
  $query = "Select * from Test where UserId =".$userId." order by Id;";
  $recSet = $conn->query($query);

  echo "<table id = 'practicePageTable' align='center'>
    <thead>
        <tr>
          <td>Test Description</td>
          <td>Status</td>
        </tr>
    </thead>
    <tbody>";
    while(list($Id, $UserId, $Info, $Completed, $Deleted) = $recSet->fetch_row())
    {
      $comp = "Failed";
      if ($Completed == "1") {
        $comp = "Passed";
      }
      echo"
      <tr>
        <td >
          $Info
        </td>
        <td >
          $comp
        </td>
      </tr>";

    }
   echo "
    </tbody>
   </table>";
}
get_tests($_SESSION['UserId']);
?>

<?php
session_start();
include_once "Connection.php";
function get_practice_pages($userId) {
	global $conn;
  $query = "Select * from PracticePage where UserId =".$userId." order by Id;";
  $recSet = $conn->query($query);

  echo "<table id = 'practicePageTable' align='center'>
    <thead>
        <tr>
          <td>Page Number</td>
          <td>Practice Page Name</td>
          <td></td>
        </tr>
    </thead>
    <tbody>";
    $counter = 1;
    while(list($Id, $UserId, $PageName, $StructureInfo, $Deleted) = $recSet->fetch_row())
    {
      echo"
      <tr>
        <td >
          $counter
        </td>
        <td >
          $PageName
        </td>
        <td >
          <form action='data_practice.php' method='post'>
              <input type='hidden' name='PracticePageId' id='PracticePageId' value=$StructureInfo ></input>
              <input type='hidden' name='structureName' id='structureName' value=$PageName ></input>
              <input type='submit' value='Go!'>
          </form>
        </td>
      </tr>";
	$counter = $counter + 1;
    }
   echo "
    </tbody>
   </table>";
}
get_practice_pages($_SESSION['UserId']);
?>

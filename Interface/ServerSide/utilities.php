<?php
include_once "Connection.php";

function mysql_fix_string($string) {
	global $conn;
	if(get_magic_quotes_gpc()) $string = stripslashes($string);
	return mysqli_real_escape_string($conn, $string);
}
?>

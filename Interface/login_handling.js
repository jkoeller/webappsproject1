$(document).ready(function(){
    $("#login").submit(function(){
        let userName = $("#username").val();
        let password = $("#password").val();

        let url  = "ServerSide/User.php";

        let data = {};
        data['func'] = "verify_user";
        data['userName'] = userName;
        data['password'] = password;

        $.post(url, data,
        function(data,status) {
            if(data === "1") {
                window.location.replace("user_menu.php");
            }
            else if(data === "0"){
              alert("User Not Found");
            }
            else{
              alert("error");
            }
        });

    });
    $("#login").submit(function(event){
        event.preventDefault();
    });
    $("#register").click(function() {
        window.location.replace("create_account.html");
    });
    $("#register").css("cursor", "pointer");
});
